const { gql } = require('apollo-server');

module.exports = gql`
  type Item {
    _id: String,
    name: String,
    author: String,
    pics:[
      String
    ],
    price: Float,
    short_desc: String,
    long_desc: String,
    date_added: Float,
    liked: Boolean,
    inCart: Boolean
  }

  type ItemBatch {
    _id: String,
    name: String,
    author: String,
    pics:[
      String
    ],
    price: Float,
    short_desc: String,
    long_desc: String,
    date_added: Float,
  }

  type PaginatedItems {
    next: Int!
    hasMore: Boolean!
    items: [ItemBatch]
  }

  type CartItemList {
    _id: String,
    customer_id: String,
    item_id: Item, # will polulate item_id becuase we need item's details
    date_added: Float 
  }

  type LikedItemList {
    _id: String,
    customer_id: String,
    item_id: Item, # will polulate item_id becuase we need item's details
    date_liked: Float 
  }
  
  type PurchasedItemList {
    _id: String,
    customer_id: String,
    item_id: Item, # will polulate item_id becuase we need item's details
    date_purchased: Float 
  }
`