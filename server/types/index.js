const { gql } = require('apollo-server');
const item = require('./item');

const query = gql`
  type Query { #done
    logInWithPassword( #done
      username: String!,
      password: String!
    ): String
    getItem(_id: String!): Item, #done
    getItemsBatch(  #done
      pageSize: Int,
      next: Int
    ): PaginatedItems!
    getCartItems:[CartItemList], #done  # if learner wants to apply paginated result to this query he/she can use the same logic as getItemsBatch query
    getLikedItems:[LikedItemList], #done
    getPurchasedItems:[PurchasedItemList], #done
  }

  type Mutation{
    signUpWithPassword(#done
      username: String!,
      password: String!,
    ):String,
    addToCart( #done
      item_id: String!
    ):Boolean
    removeItemFromCart( 
      item_id: String!
    ):Boolean
    likeItem( #done
      item_id: String!
    ):Boolean
    removeItemFromLiked( 
      item_id: String!
    ):Boolean
    purchaseItem( #done
      item_id: String!
    ):Boolean
    purchaseItemsAsBatch(ids: [String!]):Boolean #done
    modifyUserAccount( # done #when calling this mutation, if the user does not fill any field that field should be sent as an empty string to API
      new_username: String,
      old_password: String!,
      new_password: String
    ):Boolean
    removeItemFromPurchased( 
      item_id: String!
    ):Boolean
    deleteAccount( #done
      password: String!
    ):Boolean #in front end the user has to be logged out if the response from back end is true, and the toke belongs to the user has to be deleted in the front end
  }
`

module.exports = [query, item];