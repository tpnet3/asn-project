const Cart = require('../models/cart')

const getCartItems = async (obj, {}, ctx)=>{
  const carts = await Cart.getCartItems(ctx);
  if(!carts){
    throw new Error(`getCartItemsNull`)
  }
  return carts; 
}

const addToCart = async (obj, {item_id}, ctx)=>{
  const result = await Cart.addToCart(item_id, ctx);
  if(!result){
    throw new Error(`addToCartNull`)
  }
  return result; 
}

const removeItemFromCart = async (obj, {item_id}, ctx)=>{
  var result = await Cart.removeItemFromCart(item_id, ctx);
  if(!result){
    throw new Error('removeItemFromCartFailed') //this should not happen at all, but for the UX purposes and we have to be ready for this case too and properly handle this situtation as well, since no technology is 100% stable
  }
  return result;
}

module.exports = {
  getCartItems,
  addToCart,
  removeItemFromCart
}