const Purchased = require('../models/purchased')

const getPurchasedItems = async (obj, {}, ctx)=>{
  const purchaseds = await Purchased.getPurchasedItems(ctx);
  if(!purchaseds){
    throw new Error(`getPurchasedItemsNull`)
  }
  return purchaseds; 
}

const purchaseItemsAsBatch = async (obj, {ids}, ctx)=>{
  const result = await Purchased.purchaseItemsAsBatch(ids, ctx);
  if(!result){
    throw new Error(`purchaseNull`)
  }
  return result; 
}
const purchaseItem = async (obj, {item_id}, ctx)=>{
  const result = await Purchased.purchaseItem({item_id}, ctx);
  if(!result){
    throw new Error(`purchaseNull`)
  }
  return result; 
}

const removeItemFromPurchased = async (obj, {item_id}, ctx)=>{
  var result = await Purchased.removeItemFromPurchased(item_id, ctx);
  if(!result){
    throw new Error('removeItemFromPurchasedFailed') //this should not happen at all, but for the UX purposes and we have to be ready for this case too and properly handle this situtation as well, since no technology is 100% stable
  }
  return result;
}

module.exports = {
  getPurchasedItems,
  purchaseItemsAsBatch,
  purchaseItem,
  removeItemFromPurchased
}