const User = require('../models/user')

const loginWithPassword = async (obj, {username, password}, ctx)=>{
  const token = await User.CreateTokenwithPassword(username, password);
  ctx.headers.accessToken = token;
  if(token){
    ctx.headers.accessToken = token;
  }
  return token;
}

const signUpWithPassword = async (obj, {username, password}, ctx)=>{
  const token = await User.SignUpWithPassword(username, password);
  ctx.headers.accessToken = token;
  return token;
}
const modifyUserAccount = async (obj, {new_username, old_password, new_password}, ctx)=>{
  const result = await User.modifyUserAccount({new_username, old_password, new_password}, ctx);
  if(!result){
    return false
  }
  return result
} 
const deleteAccount = async (obj, {password}, ctx)=>{
  var result = await User.deleteAccount(password, ctx);
  if(!result){
    return false
  }
  return result;
}

module.exports = {
  loginWithPassword,
  signUpWithPassword,
  modifyUserAccount,
  deleteAccount
}