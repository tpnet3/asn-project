const Liked = require('../models/liked')

const getLikedItems = async (obj, {}, ctx)=>{
  const likeds = await Liked.getLikedItems(ctx);
  if(!likeds){
    throw new Error(`getLikedItemsNull`)
  }
  return likeds; 
}

const likeItem = async (obj, {item_id}, ctx)=>{
  const result = await Liked.addToLiked(item_id, ctx);
  if(!result){
    throw new Error(`addToLikedNull`)
  }
  return result; 
}

const removeItemFromLiked = async (obj, {item_id}, ctx)=>{
  var result = await Liked.removeItemFromLiked(item_id, ctx);
  if(!result){
    throw new Error('removeItemFromLikedFailed') //this should not happen at all, but for the UX purposes and we have to be ready for this case too and properly handle this situtation as well, since no technology is 100% stable
  }
  return result;
}

module.exports = {
  getLikedItems,
  likeItem,
  removeItemFromLiked
}