const mongoose = require('mongoose');

const likedSchema = new mongoose.Schema({
  customer_id: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  item_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Item'},
  date_liked: Number 
})

likedSchema.statics.getLikedItems = async function (ctx) {
  if(ctx.headers.accessToken==null){
    throw new Error(`tokenFailed`);
  }
  const user = await this.model('User').verifyUser(ctx.headers.accessToken);
  if(!user){
    throw new Error(`tokenFailed`)
  }
  var result = await this.find({customer_id: user._id}).populate('item_id').exec();//explain populate well with examples
  if(!result){
    throw new Error(`getCartItemsFailed`) //this should not happen at all, but for the UX purposes and we have to be ready for this case too and properly handle this situtation as well, since no technology is 100% stable
  }
  else{
    result = result.reverse() // this is used to return reverse order of the fetched array and this will result in date_added in descending order, this means latest first
    return result;
  }
}

likedSchema.statics.addToLiked = async function (item_id, ctx) {
  if(ctx.headers.accessToken==null){
    throw new Error(`tokenFailed`);
  }
  const user = await this.model('User').verifyUser(ctx.headers.accessToken);
  if(!user){
    throw new Error(`tokenFailed`)
  }
  var inliked = await this.inLiked(item_id, user._id);
  if(inliked!=null){
    throw new Error("alreadyInLiked");
  }
  
  var result = this.create({
    customer_id: user._id,
    item_id: item_id,
    date_liked: Date.now()
  })

  if(!result){
    return false;
  }
  else{
    return true;
  }
}

likedSchema.statics.inLiked = async function (item_id, user_id) {
  return this.findOne({$and: [{item_id: item_id}, {customer_id: user_id}]}); //fetching products in cart according to item_id and custormer_id
}

likedSchema.statics.removeItemFromLiked = async function (item_id,ctx) {
  if(ctx.headers.accessToken==null){
    throw new Error(`tokenFailed`);
  }
  const user = await this.model('User').verifyUser(ctx.headers.accessToken);
  if(!user){
    throw new Error(`tokenFailed`)
  }
  var result = await this.findOneAndDelete({$and: [{item_id: item_id}, {customer_id: user._id}]}).exec();
  if(!result){
    throw new Error('itemNotFoundInLiked') //If the user presses delete twice or more in a split second, this condition might happen
  }
  return true;
}

module.exports = mongoose.model('Liked', likedSchema);