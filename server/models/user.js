const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const passwordCrypto = require('../lib/passwordCrypto');
const secret = process.env.SECRET || 'secret';

const userSchema = new mongoose.Schema({
  username: String,
  password: String,
});

userSchema.statics.CreateTokenwithPassword = async function (username, password) { //I have called this function CreateTokenwithPassword because later we might add login with facebook, ok.ru or any other social networks
  const user = await this.findOne({username: username}).exec();
  
  if(!user){
    throw new Error("userNotFound");
  }
  
  if(user.password === passwordCrypto(password)){//actually from front end this should come as encrypted with passwordCrypto()
    //time to make access token 
    const expiresIn = 7776000;
    const accessToken = jwt.sign({
      _id: user._id,
    }, secret, {expiresIn: expiresIn}); //made token
    
    return accessToken;
  }
  
  else{
    throw new Error("passwordIncorrect");
  }
}

userSchema.statics.verifyUser = async function (token) {    //This is not a resolver function. This is helper function
  var res = await jwt.verify(token, secret);
  
  if(!res){
    return false;
  }
  
  var res = await this.findById({_id: res._id}).exec();
  
  if(!res){
    return false
  }
  
  return res;
}

userSchema.statics.SignUpWithPassword = async function (username, password) {
  const userName = await this.findOne({username: username}).exec();
  
  if(userName){
    throw new Error(`usernameTaken`);
  }
  
  const user = await this.create({
    username: username,
    password: passwordCrypto(password),
  });
  
  if(!user){
    throw new Error("signUpFailed");
  }
  
  //time to make access token 
  const expiresIn = 7776000;
  
  const accessToken = jwt.sign({
    _id: user._id,
  }, secret, {expiresIn: expiresIn}); //made token
  
  return accessToken;
}

userSchema.statics.modifyUserAccount = async function ({new_username, old_password, new_password}, ctx) {
  var flag = true;
  
  if(ctx.headers.accessToken==null){
    throw new Error(`tokenFailed`);
  }
  
  const user = await this.verifyUser(ctx.headers.accessToken);
  
  if(!user){
    throw new Error(`tokenFailed`)
  }
  
  if(user.password !== passwordCrypto(old_password)){
    throw new Error(`oldPasswordIncorrect`)
  }
  
  if(!(new_username==='')){//user wants to change the username
    const userName = await this.findOne({username: new_username}).exec();
    
    if(userName){
      throw new Error(`usernameTaken`);//new username is taken
    }
    
    if(!(await this.findByIdAndUpdate({_id: user._id}, {$set: {username: new_username}}).exec())){
      throw new Error(`usernameUpdateFailed`);//new username can not be updated
    }
  }
  
  if(!(new_password==='')){//user wants to change password
    if(!(await this.findByIdAndUpdate({_id: user._id}, {$set: {password: passwordCrypto(new_password)}}).exec())){
      throw new Error(`passwordUpdateFailed`);//new username can not be updated
    }
  }
  
  return flag
}

userSchema.statics.deleteAccount = async function (password, ctx) {
  if(ctx.headers.accessToken==null){
    throw new Error(`tokenFailed`);
  }
  
  const user = await this.verifyUser(ctx.headers.accessToken);
  
  if(!user){
    throw new Error(`tokenFailed`)
  }
  
  if(user.password!==passwordCrypto(password)){
    throw new Error(`passwordIncorrect`)
  }
  
  //before deleting the user account we have to delete everything belonging to the particular user. This include removing cart, likes, and purchases
  res = await this.model('Cart').deleteMany({customer_id: user._id}).exec();
  
  if(!res){
    throw new Error(`deleteCartFailed`) //this should not happen at all, but for the UX purposes and we have to be ready for this case too and properly handle this situtation as well, since no technology is 100% stable
  }
  
  res = await this.model('Liked').deleteMany({customer_id: user._id}).exec();
  
  if(!res){
    throw new Error(`deleteLikedFailed`) //this should not happen at all, but for the UX purposes and we have to be ready for this case too and properly handle this situtation as well, since no technology is 100% stable
  }
  
  res = await this.model('Purchased').deleteMany({customer_id: user._id}).exec();
  
  if(!res){
    throw new Error(`deletePurchasedFailed`) //this should not happen at all, but for the UX purposes and we have to be ready for this case too and properly handle this situtation as well, since no technology is 100% stable
  }
  
  res = await this.findByIdAndDelete({_id: user._id}).exec();
  
  if(!res){
    throw new Error(`deleteAccountFailed`) //this should not happen at all, but for the UX purposes and we have to be ready for this case too and properly handle this situtation as well, since no technology is 100% stable
  }
  
  return true;
}

module.exports = mongoose.model('User', userSchema);