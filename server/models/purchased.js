const mongoose = require('mongoose');

const purchasedSchema = new mongoose.Schema({
  customer_id: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  item_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Item'},
  date_purchased: Number
})

purchasedSchema.statics.getPurchasedItems = async function (ctx) {
  if(ctx.headers.accessToken==null){
    throw new Error(`tokenFailed`);
  }
  
  const user = await this.model('User').verifyUser(ctx.headers.accessToken);
  
  if(!user){
    throw new Error(`tokenFailed`)
  }
  
  var result = await Purchased.find({customer_id: user._id}).populate('item_id').exec();//explain populate well with examples
  
  if(!result){
    throw new Error(`getCartItemsFailed`) //this should not happen at all, but for the UX purposes and we have to be ready for this case too and properly handle this situtation as well, since no technology is 100% stable
  }
  
  else{
    result = result.reverse() // this is used to return reverse order of the fetched array and this will result in date_added in descending order, this means latest first
    return result;
  }
}

purchasedSchema.statics.purchaseItemsAsBatch = async function (ids, ctx) {
  var flag = true;
  
  if(ctx.headers.accessToken==null){
    throw new Error(`tokenFailed`);
  }
  
  const user = await this.model('User').verifyUser(ctx.headers.accessToken);
  
  if(!user){
    throw new Error(`tokenFailed`)
  }
  
  var inpurchased;
  var result;
  
  for(let i = 0; i< ids.length; i++){
    inpurchased = await this.inPurchased(ids[i], user._id);
    
    if(inpurchased!=null){
      continue; // if and item is already in purchase we should skip that item, this happens very rarely, and actually should not happen at all
    }
    
    this.deleteItemFromCart(ids[i], user._id);//this will delete the item from cart if it exists in user's cart
    
    result = this.create({
      customer_id: user._id,
      item_id: ids[i],
      date_purchased: Date.now()
    })
    
    if(!result){
      throw new Error(ids[i]); //if an id of an item is thrown as an error we should know that ID had failed to be purchased, more clearly Mongoose failed to put it to mongoDB, actually this hould not happen at all 
    }
    
    else{
      flag = true;
    }
  }
  return flag;
}

purchasedSchema.statics.purchaseItem = async function ({item_id}, ctx) {
  if(ctx.headers.accessToken==null){
    throw new Error(`tokenFailed`);
  }
  
  const user = await this.model('User').verifyUser(ctx.headers.accessToken);
  
  if(!user){
    throw new Error(`tokenFailed`)
  }
  
  var inpurchased = await this.inPurchased(item_id, user._id);
  
  if(inpurchased!=null){
    throw new Error("alreadyInPurchased");
  }
  
  this.deleteItemFromCart(item_id, user._id);//this will delete the item from cart if it exists in user's cart
  
  var result = this.create({
    customer_id: user._id,
    item_id: item_id,
    date_purchased: Date.now()
  })
  
  if(!result){
    return false;
  }
  
  else{
    return true;
  }
}

purchasedSchema.statics.inPurchased = async function (item_id, user_id) {//this is helper function 
  return Purchased.findOne({$and: [{item_id: item_id}, {customer_id: user_id}]}); //fetching products in cart according to item_id and custormer_id
}

purchasedSchema.statics.deleteItemFromCart = async function (item_id, user_id) {//this is helper function
  return this.model('Cart').findOneAndDelete({$and: [{item_id: item_id}, {customer_id: user_id}]}); //fetching products in cart according to item_id and custormer_id
}

purchasedSchema.statics.removeItemFromPurchased = async function (item_id,ctx) {
  if(ctx.headers.accessToken==null){
    throw new Error(`tokenFailed`);
  }
  
  const user = await this.model('User').verifyUser(ctx.headers.accessToken);
  
  if(!user){
    throw new Error(`tokenFailed`)
  }
  
  this.deleteItemFromCart(item_id, user._id);//this will delete the item from cart if it exists in user's cart
  
  var result = await Purchased.findOneAndDelete({$and: [{item_id: item_id}, {customer_id: user._id}]}).exec();
  
  if(!result){
    throw new Error('itemNotFoundInPurchased') //If the user presses delete twice or more in a split second, this condition might happen
  }
  return true;
}

module.exports = mongoose.model('Purchased', purchasedSchema);