import gql from 'graphql-tag'

export const ADD_TO_LIKED = gql`
    mutation likeItem($item_id: String!){
        likeItem(item_id: $item_id)
    }
`
export const REMOVE_FROM_LIKED = gql`
    mutation removeItemFromLiked($item_id: String!){
        removeItemFromLiked(item_id: $item_id)
    }
`