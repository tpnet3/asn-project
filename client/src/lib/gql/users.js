import gql from "graphql-tag";

export const LOG_IN_WITH_PASSWORD = gql`
query logInWithPassword($username: String!, $password: String!){
  logInWithPassword(username: $username, password: $password)
}
`

export const SIGN_UP_WITH_PASSWORD = gql`
mutation signUpWithPassword($username: String!, $password: String!){
  signUpWithPassword(username: $username, password: $password)
}
`

export const MODIFY_USER_ACCOUNT = gql`
  mutation modifyUserAccount($new_username:String, $old_password: String!, $new_password: String){
    modifyUserAccount(new_username: $new_username, old_password: $old_password, new_password: $new_password)
  }
`

export const DELETE_ACCOUNT = gql`
  mutation deleteAccount($password: String!){
    deleteAccount(password: $password)
  }
`