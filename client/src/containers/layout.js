import React from 'react'
import Helmet from 'react-helmet'



import './layout.css'

const Layout = ({ children, title }) => (
      <>
        <Helmet
          title={title? title: 'BounceCode BookStore'}
          meta={[
            { name: 'description', content: 'Sample' },
            { name: 'keywords', content: 'sample, something' },
          ]}
        >
          <html lang="en" />
        </Helmet>
          {children}
      </>
)

export default Layout
