import React, { Component } from 'react'
import styled from 'styled-components'

const ErrorWrapper = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    img{
        width: 300px;
        height: auto;
    }
`

export default class Error extends Component {
  render() {
    return (
      <ErrorWrapper>
          <img alt="Error" src={require('./../images/error.png')}/>
          <p>{this.props.children}</p>
      </ErrorWrapper>
    )
  }
}
