import React from 'react'
import styled from 'styled-components'
import ImageCarusel from './ImageCarusel'

const Card = styled.div `
    width: 400px;
    height: 540px;
    position: relative;
    overflow: hidden;
    border-radius: 0 10px 10px 0;
    box-shadow: 0 20px 40px rgba(0,0,0,0.25);
    transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);
    cursor: pointer;

    &:hover {
        transform: scale(1.01, 1.01);
        box-shadow: 0 30px 60px rgba(0,0,0,0.5);
    }
    @media screen and (max-width: 640px) {
        width: 300px;
        height: 400px;
    }
`



const SingleItem = props => (
    <Card>
        <ImageCarusel images={props.images} />
    </Card>
) 

export default SingleItem
