import React, { Component } from "react";
import styled from 'styled-components'
import Button from './button'


const Container = styled.div`
  width: 550px;
  height: 620px;
  position: relative;
  overflow: hidden;
  border-radius: 20px;
  /* box-shadow: 0 20px 40px rgba(0,0,0,0.25); */
  display: grid;
  background: none;
  justify-content: center;
  margin: 0 auto;
  grid-template-rows: 1fr 1fr;
  transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);
  cursor: pointer;
  @media screen and (max-width: 720px) {
    width: 350px;
    height: 550px;
    }
`

const Label = styled.label `
  font-size: 30px;
  background: linear-gradient(104deg, #050A27 0%, #4A548C 100%);
  -webkit-background-clip: text;
  background-clip: text;
  font-weight: bold;
  margin-bottom: 10px;
  display: block;
  @media screen and (max-width: 720px) {
    font-size: 20px;
  }
`
const Input = styled.input `
  background: rgba(1, 15, 43, .9);
  border: 1px solid rgba(255, 255, 255, 0.15);
  box-sizing: border-box;
  backdrop-filter: blur(40px);
  border-radius: 14px;
  height: 60px;
  width: 400px;
  color: white;
  font-size: 24px;
  padding: 18px 20px;

  ::placeholder {
    font-size: 24px;
  }
  @media screen and (max-width: 720px) {
    width: 300px;
    height: 40px;
    font-size: 18px;
    }
    ::placeholder {
    font-size: 18px;
  }
`
const Field = styled.div `
  margin-top: 50px;
  margin-bottom: 0 !important; 
  p{
    color: red;
    font-size: 10px;
  }
`



class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      newPassword: "",
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleNewPasswordChange = this.handleNewPasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  
  handleNameChange(event) {
    this.setState({ username: event.target.value });
  }

  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  handleNewPasswordChange(event) {
    this.setState({ newPassword: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.commitSubmit(this.state.username, this.state.password, this.state.newPassword);
  }

  render() {

    const { BtnName, TextLabel, PassLabel, PassLabel2, BtnName2 } = this.props;
    const { username, password, newPassword } = this.state;

    return (
      <Container>
        <form className="form" onSubmit={this.handleSubmit}>
          <Field>
            <Label className="Label">{TextLabel}</Label>
            <Input
              className="input"
              type="text"
              name="username"
              placeholder="Username"
              value={username}
              onChange={this.handleNameChange}
            />
            {this.props.usernameTaken?<p>Username already exists</p>:null}
          </Field>

          <Field>
            <Label className="Label">{PassLabel}</Label>
            <Input
              className="input"
              type="password"
              name="password"
              placeholder="Password"
              value={password}
              onChange={this.handlePasswordChange}
            />
            {this.props.oldPasswordIncorrect?<p>You entered wrong current password</p>:null}
          </Field>

          <Field>
            <Label className="Label">{PassLabel2}</Label>
            <Input
              className="input"
              type="password"
              name="password"
              placeholder="Password"
              value={newPassword}
              onChange={this.handleNewPasswordChange}
            />
          </Field>

          <Button
            background="#56CCF2"
            fontSize="24px"
            borderRadius="12px"
            mt="45px"
            color="white"
            width="150px"
            height="45px">

            {BtnName}</Button>
            <Button
            type="button" //this should be button type because this button should not submit the form 
            onClick={()=>{this.props.toggleModal()}}
            background="#E1467C"
            fontSize="24px"
            borderRadius="12px"
            mt="45px"
            ml="30px"
            color="white"
            width="150px"
            height="45px">
            {BtnName2}</Button>
        </form>
      </Container>
    );
  }
}

export default Form;
