import React, { Component } from 'react'
import styled from 'styled-components'

const CaruselWrapper = styled.div`
    width: 100%;
    height: 100%;
    position: relative;
    img {
        width: 100%;
        height: 100%;
    }
`

const CircleWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: row;
    position: absolute;
    background-color: rgba(151,151,151,0.5);
    bottom: 20px;
    border-radius: 10px;
    left: 40%;
`

const Circle = styled.button`
    width: 15px;
    margin: 5px;
    height: 15px;
    border: 1px solid rgb(50, 50, 50);
    border-radius: 50%;
    bottom: 20px;
`
export default class ImageCarusel extends Component {
    constructor(props){
        super();
        this.state={
            onScreen: props.images[0] || 'http://helideckinspect.com/v4/wp-content/uploads/2018/01/no_image.png' // is porps has no image this "No image" image will be displayed
         }
    }
   setImage = (image)=>{
       this.setState({onScreen: image})
    }
    onImgClickNext = ()=>{
        let len = this.props.images.length;
        let currentIndex = this.props.images.indexOf(this.state.onScreen);
        this.setState({
            onScreen: this.props.images[(currentIndex+1)%len]
        })
    }
  render() {
    return (
      <CaruselWrapper>
          <img onClick={this.props.images.length>0?()=>this.onImgClickNext():()=>{}} alt="item pic" src={this.state.onScreen} />
          {this.props.images.length>1?
          <CircleWrapper>
          {this.props.images.map(images=>
          <Circle key={images} onClick={()=>this.setImage(images)} style={{backgroundColor: this.state.onScreen===images?'rgb(50, 50, 50)':'rgba(100, 100, 100, 0)'}} />
            )}
            </CircleWrapper>:null}
      </CaruselWrapper>
    )
  }
}
