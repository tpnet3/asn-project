import styled from 'styled-components'
import React, { Component } from 'react'
import {Mutation} from 'react-apollo'
import {ADD_TO_LIKED} from '../lib/gql/like'
import {REMOVE_FROM_LIKED} from '../lib/gql/like'


const Button = styled.button`
    /* background: linear-gradient(102.24deg, #9B51E0 0%, #3436E7 100%); */
    background: linear-gradient(102.24deg, #9B51E0 0%, #3436E7 100%);
    box-shadow: 0px 10px 20px rgba(101, 41, 255, 0.15);
    border-radius: 30px;
    
    /* paddings */
    color: white;
    border: none;
    min-width: 100px;
    min-height: 40px;
    font-weight: 600;
    font-size: 10px;
    justify-self: center;
    font-weight: bold;
    transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);
    &:hover {
        box-shadow: 0 20px 40px rgba(0,0,0, 0.15);
        transform: translateY(-3px);
    }
`

export default class CartButton extends Component {
    constructor(){
        super()
        this.state={
            inLiked: false,
            loggedIn: false
        }
    }
    componentWillMount(){
        this.setState({inLiked: this.props.inLiked})
        if(localStorage.getItem('accessToken')!==''){
            this.setState({
                loggedIn: true
            })
        }
    }
  render() {
    return (
        this.state.inLiked?
        <Mutation mutation={REMOVE_FROM_LIKED}
        onCompleted={data => {
        if (data.removeItemFromLiked) {
            this.setState({
                inLiked: false
            });
        }
      }}
      onError={error => {
          console.log(error)
      }}
      >
      {(removeItemFromLiked, { data, error }) => (
          <Button 
            onClick={()=>{
                removeItemFromLiked({ variables: { item_id: this.props.item_id }});
            }}>Unlike</Button>
      )
      }
        </Mutation>:
        <Mutation mutation={ADD_TO_LIKED}
        onCompleted={data => {
        if (data.likeItem) {
            this.setState({
                inLiked: true
            });
        }
      }}
      onError={error => {
          console.log(error)
      }}
      >
      {(likeItem, { data, error }) => (
          <Button 
            onClick={()=>{
                if(this.state.loggedIn){
                    likeItem({ variables: { item_id: this.props.item_id }});
                }
                else{
                    this.props.pushHistory()
                }
            }}>Like</Button>
      )
      }
        </Mutation>
    )
  }
}
