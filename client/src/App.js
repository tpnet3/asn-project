import React, { Component } from 'react';
import Loadable from 'react-loadable';
import { Switch, Route } from 'react-router-dom'
import Loading from './components/loading'

const Items = Loadable({ loader: () => import('./pages/items'), loading: Loading});
const Cart = Loadable({ loader: () => import('./pages/cart'), loading: Loading});
const Item = Loadable({ loader: () => import('./pages/item'), loading: Loading});
const Profile = Loadable({ loader: () => import('./pages/profile'), loading: Loading});
const SignUp = Loadable({ loader: () => import('./pages/signup'), loading: Loading});
const Login = Loadable({ loader: () => import('./pages/login'), loading: Loading});
const NotFound = Loadable({ loader: () => import('./pages/404'), loading: Loading});


class App extends Component {
  render() {
    return (
      <main>
        <Switch>
          <Route exact path='/' component={Items}/>
          <Route exact path='/cart' component={Cart}/>
          <Route exact path='/item/:id' component={Item}/>
          <Route exact path='/profile' component={Profile}/>
          <Route exact path='/signup' component={SignUp}/>
          <Route exact path='/login' component={Login}/>
          <Route path='/' component={NotFound}/>
        </Switch>
      </main>
    );
  }
}

export default App;
