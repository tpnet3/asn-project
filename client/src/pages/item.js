import React, { Component } from 'react';
import SingleItem from '../components/singleItem'
import Layout from '../containers/layout'
import Header from '../containers/header'
import Footer from '../containers/footer'
import ItemDescription from '../components/ItemDescription'
import Loading from './../components/loading'
import Error from './../components/error'
import Button from '../components/button'
import { Query } from "react-apollo";
import {GET_ITEM}  from './../lib/gql/items'
import CartButton from './../components/cartbutton'
import LikeButton from './../components/likebutton'

class Item extends Component {
  constructor(){
    super();
    this.state={
     liked: false,
     inCart: false
    }
  }
  render() {
    const data  = this.props.match.params
    const variables = { 
      _id: data.id
    }
    return (
      <Query query={GET_ITEM} variables={variables}>
      {({ loading, error, data }) => {
      if (loading) return <Loading />
      if (error) {return <Error>{error.message}</Error>;}
        if(data){
      return(
      <Layout>
        <Header />
        <div className="SingleItem">
          <h2>Single Item</h2>
       
          <div className="SingleItems">
          <div>
           <SingleItem
              images={data.getItem.pics}
           />
           <div className="ButtonGroup">
              <CartButton 
                item_id = {variables._id}
                inCart = {data.getItem.inCart}
                pushHistory = {()=> {
                  alert("Please login first")
                  this.props.history.push('/login')
                }}
              />
              <LikeButton 
                item_id = {variables._id}
                inLiked = {data.getItem.liked}
                pushHistory = {()=> {
                  alert("Please login first")
                  this.props.history.push('/login')
                }}
              />
              <Button 
              background="linear-gradient(102.24deg, #9B51E0 0%, #3436E7 100%)"
              width = "100px"
              height = "40px"
              color = "white"
              fontSize = "15px"
              borderRadius = "30px"
              >Buy</Button>
            </div></div>
           <ItemDescription
              bookname={data.getItem.name}
              author={data.getItem.author}
              price = {data.getItem.price}
              discount = {0.1}
              short_desc={data.getItem.short_desc}
              long_desc={data.getItem.long_desc}
           />
          </div>
        </div>
        <Footer/>
      </Layout>
      )}}}
      </Query>
    );
  }
}

export default Item;