import React, { Component } from 'react';
import Card from '../components/card'
import Loading from './../components/loading'
import Layout from '../containers/layout'
import Header from '../containers/header'
import Footer from '../containers/footer'
import { Query } from "react-apollo";
import {GET_ITEMS_BATCH}  from './../lib/gql/items'

class Items extends Component {
  constructor(){
    super();
    this.state={
      pageSize: 16,
      next: 1
    }
  }
  render() {
    const variables = { 
      pageSize: this.state.pageSize,
      next: this.state.next
    }
    return (
      <Query query={GET_ITEMS_BATCH} variables={variables}>
      {({ loading, error, data }) => {
      if (loading) return <Loading />;
      if (error) {console.log(error); return <p>Error :(</p>;}
        console.log(data)
      return(
      <Layout>
        <Header onFocus={1} />
        <div className="Cards">
          <h2>All Items</h2>
          <div className="CardGroup">
          {data.getItemsBatch.items.map((items)=>{
            return (
              <Card
              title={items.name}
              images={items.pics.length>0?items.pics[0]:[]}
              price={items.price}
              discount={0.1}
              to = "item"
              data={items._id}
            />
            )
          })}
          </div>
        </div>
        <Footer />
      </Layout>
      )}}
      </Query>
    );
  }
}

export default Items;