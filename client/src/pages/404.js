import React, { Component } from 'react';
import Layout from '../containers/layout'
import Header from '../containers/header'
import Footer from '../containers/footer'

class Profile extends Component {
  constructor(){
    super();
    this.state = {
      modalOpen: false
    }
  }
  render() {
    return (
      <Layout>
        <Header onFocus={3} />
        <div className="SingleItem">
        <h2>404 Page Not Found</h2>
        </div>
        <Footer/>
      </Layout>
    );
  }
}

export default Profile;