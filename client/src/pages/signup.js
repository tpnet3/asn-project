import React, { Component } from 'react';
import Layout from '../containers/layout'
import Header from './../containers/header'
import Footer from './../containers/footer'
import ProfileForm from '../components/form'
import { Mutation } from "react-apollo";
import {SIGN_UP_WITH_PASSWORD} from './../lib/gql/users'

class SignUp extends Component {
  constructor(){
    super();
    this.state = {
      usernameTaken: false
    }
  }
  render() {
    return (
      <Mutation mutation={SIGN_UP_WITH_PASSWORD}
      onCompleted={data => {
        // Store the token in cookie
        if (data.signUpWithPassword) {
          localStorage.setItem('accessToken', data.signUpWithPassword);
          this.props.history.push('/');
        }
      }}
      onError={error => {
        if(error.message==='GraphQL error: usernameTaken'){
          this.setState({usernameTaken: true})
        }
      }}
      >
      {(signUpWithPassword, { data, error }) => (
        <Layout>
          <Header />
        <div className="SingleItem">
          <h2>Sign Up</h2>
       
          <div className="Profile">
           <ProfileForm
            TextLabel="User Name"
            PassLabel="Password"
            usernameTaken = {this.state.usernameTaken}
            BtnName="SignUp"
            commitSubmit={(username, password)=>{
              signUpWithPassword({ variables: { username: username, password: password }});
            }}
           />
          </div>
        </div>
        <Footer />
      </Layout>
      )
      }
    </Mutation>
    );
  }
}
export default SignUp;

